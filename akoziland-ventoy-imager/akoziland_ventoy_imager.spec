Name:		akoziland-ventoy-imager
Version:        1.0
Release:        1%{?dist}
Summary:        Automated ventoy installer, accepts the same arguements as normal ventoy
URL:            https://github.com/sirmrgentleman/akoziland-ventoy-imager
Source0:        https://github.com/sirmrgentleman/akoziland-ventoy-imager/releases/download/latest/akoziland-ventoy-imager.tar.gz
BuildArch:      noarch
License:        MIT
Requires:       curl gzip tar bash

%description 
RPM spec file for akoziland ventoy imager

%define debug_package %{nil}

%prep
%setup -T -b 0 -q -n akoziland-ventoy-imager

%install
ls
pwd
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
cp %{name} $RPM_BUILD_ROOT/%{_bindir}

%files
%license LICENSE
%{_bindir}/%{name}
